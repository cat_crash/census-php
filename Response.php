<?php
namespace cat_crash\census_php;

Class Response {
	
	public $result;

	public function __construct(array $body){
		if(array_key_exists('result', $body)){
			$this->result=new Result($body['result']);

		} else {
			throw new \Exception("Response invalid");
		}


	}

}