<?php
namespace cat_crash\census_php;

Class AddressComponents {
		
	public $fromAddress;
	public $toAddress;
	public $preQualifier;
	public $preDirection;
	public $preType;
	public $streetName;
	public $suffixType;
	public $suffixDirection;
	public $suffixQualifier;
	public $city;
	public $state;
	public $zip;


	public function __construct(array $body){
		
		foreach($body as $key=>$value){
			$this->setProperty($key,$value);
		} 
		return $this;
	}

	public function setProperty($name,$value){
		if(property_exists($this, $name)){
			$this->{$name}=$value;
		}
	}

}