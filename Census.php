<?php
namespace cat_crash\census_php;

//require('/Users/valery/www/mapkin/www/vendor/autoload.php');

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;
  
Class Census {
	
	public $baseApiUri='https://geocoding.geo.census.gov/geocoder';
	public $returntype='locations';
	public $searchtype='onelineaddress';

	public $searchString='';

	public $verbose=false;

	private $client;
	private $format='json';
	private $layers='';
	private $benchmark='2020'; //https://geocoding.geo.census.gov/geocoder/benchmarks 

	private $results;
	

	public function __construct($settings=null){

		$stack = HandlerStack::create();
		$stack->push(
		    Middleware::log(
		        new Logger('Logger'),
		        new MessageFormatter('{uri} - {code} - {req_body} - {res_body}')
		    )
		);


		$this->client = new Client([
		    // Base URI is used with relative requests
		    'base_uri' => $this->baseApiUri,
		    // You can set any number of default request options.
		    'timeout'  => 10.0,
		    'handler' => ($this->verbose)?$stack:null,
		]);

		if(isset($settings['benchmark'])){
			$this->setBenchmark($settings['benchmark']);
		}

		if(isset($settings['returntype'])){
			$this->setReturntype($settings['returntype']);
		}

		if(isset($settings['searchtype'])){
			$this->setSearchtype($settings['searchtype']);
		}

	}

	
	
	public function setReturntype($returntype){
		if(!in_array($returntype, ['locations','geographies'])){
			throw new \Exception('returntype should be either locations or geographies. ['.$returntype.'] provided');
		}
		$this->returntype=$returntype;
		return $this;
	}

	public function setSearchtype($searchtype){
		if(!in_array($searchtype, ['onelineaddress','address','coordinates'])){
			throw new \Exception('searchtype should be either onelineaddress or address or coordinates.['.$searchtype.'] provided');
		}
		$this->searchtype=$searchtype;

		return $this;
	}

	public function setBenchmark($benchmark){
		$this->benchmark=$benchmark;
	}

	public static function json2obj($text){
		return json_decode($text,true);
	}

	public function search($data){
		if(is_array($data) and array_key_exists('street', $data)){
			$this->setSearchtype('address');
			$query=['benchmark'=>$this->benchmark,'format'=>$this->format];
			$query=array_merge($data,$query);
		} elseif (is_array($data) and array_key_exists('x', $data) && array_key_exists('y', $data)) {
			$this->setSearchtype('coordinates');
			$this->setReturntype('geographies');
			$query=['benchmark'=>$this->benchmark,'format'=>$this->format];
			$query=array_merge($data,$query);
		}else{
			$this->setSearchtype('onelineaddress');
			$query=['address'=>$data,'benchmark'=>$this->benchmark,'format'=>$this->format,'layers'=>$this->layers];
		}


		$response = $this->client->get($this->baseApiUri.'/'.$this->returntype.'/'.$this->searchtype,['query'=>$query]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
		}
		$body=$response->getBody();
		$tmp=self::json2obj($body);
		$return=new Response($tmp);

		$this->results=$return;

		return $this;
	}

	public function getResults(){
		return $this->results;
	}


	public function getParsedAddresses(){

		return $this->results->result;

	}




	/*

	public function CreateBusinesses(Business $business){
		$this->checkCertAndPass();

		$business=$business->getBusiness();
		$identity=$this->identity->getIdentity();

		$resource=[
			"Businesses"=>[$business],
			"TrackingId"=>$this->trackingId,
			"Identity"=>$identity
		];
	
		$response=$this->client->request('POST', 'CreateBusinesses',['cert'=>[$this->certificate,$this->password],'json' => $resource]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
			return null;
		}

		$object=json_decode($response->getBody()->getContents(),true);
		
		if(is_array($object) && array_key_exists('TrackingId', $object)){
			$this->setTrackingId($object['TrackingId']);
		}

		return $object;
		
	}

	public function setTrackingId($id){
		$this->trackingId=$id;
	}


	public function UpdateBusinesses(Business $business){
		$this->checkCertAndPass();

		$business=$business->getBusiness();
		$identity=$this->identity->getIdentity();
		
		$resource=[
			"Businesses"=>[$business],
			"TrackingId"=>$this->trackingId,
			"Identity"=>$identity
		];
	
		$response=$this->client->request('POST', 'UpdateBusinesses',['cert'=>[$this->certificate,$this->password],'json' => $resource]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode(),true);
			return null;
		}

		return json_decode($response->getBody()->getContents(),true);

	}



	public function GetBusinessesByAttributes($_attributes){
		if(array_key_exists('BusinessName', $_attributes) || array_key_exists('City', $_attributes) || array_key_exists('BPCategoryId', $_attributes) || array_key_exists('Zip', $_attributes)){
			$_array=["PageNumber"=>1,
			 	"PageSize"=>100,
			 	"SearchCriteria"=>[
					 "CriteriaType"=>"SearchByQuery",
				 ],	
			 	"TrackingId"=> $this->trackingId
				];

				$_array["SearchCriteria"]=$_array["SearchCriteria"]+$_attributes;

			return $this->GetBusinessesBase($_array);

		} else {
			throw new \Exception("BusinessName and (or) City and (or) BPCategoryId and (or) Zip have to be defined search array", 1);
		}
		
	}



	public function GetBusinessesByStoreId($storeId){
		if(is_string($storeId)){
			$StoreIds[]=$storeId;
		} else {
			$StoreIds=$storeId;
		}

		$_array=["PageNumber"=>1,
			 	"PageSize"=>100,
			 	"SearchCriteria"=>[
					 "CriteriaType"=>"SearchByStoreIds",
					 "StoreIds"=> $StoreIds,
				 ],	
			 	"TrackingId"=> $this->trackingId
				];

		return $this->GetBusinessesBase($_array);
	}

	public function GetBusinessesBatches(){
		$_array=["PageNumber"=>1,
			 	"PageSize"=>100,
			 	"SearchCriteria"=>[
					 "CriteriaType"=>"GetInBatches",
				 ],	
			 	"TrackingId"=> $this->trackingId
				];
		return $this->GetBusinessesBase($_array);
	}

	public function GetBusinessesBase($_array){
		$this->checkCertAndPass();
		$identity=$this->identity->getIdentity();
		$resource=$_array+["Identity"=>$identity];

		$response=$this->client->request('POST', 'GetBusinesses',['cert'=>[$this->certificate,$this->password],'json' => $resource]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode(),true);
			return null;
		}

		return json_decode($response->getBody()->getContents());

	}

	public function GetBusinessStatusInfo($_array){

		$this->checkCertAndPass();
		$identity=$this->identity->getIdentity();
		$resource=$_array+["Identity"=>$identity];
	
		$response=$this->client->request('POST', 'GetBusinessStatusInfo',['cert'=>[$this->certificate,$this->password],'json' => $resource]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
			return null;
		}

		return json_decode($response->getBody()->getContents(),true);

	}


	public function GetAnalyticsByStoreId($storeId){
		if(is_string($storeId)){
			$StoreIds[]=$storeId;
		} else {
			$StoreIds=$storeId;
		}

		$_array=["PageNumber"=>1,
			 	"PageSize"=>100,
				"CriteriaType"=>"SearchByStoreIds",
				"StoreIds"=> $StoreIds,
			 	"TrackingId"=> $this->trackingId
				];

		return $this->GetAnalyticsBase($_array);
	}

	public function GetAnalyticsBatches(){
		$_array=["PageNumber"=>1,
			 	"PageSize"=>100,
				"CriteriaType"=>"GetInBatches",
			 	"TrackingId"=> $this->trackingId
				];
		return $this->GetAnalyticsBase($_array);
	}

	public function GetAnalyticsBase($_array){

		$this->checkCertAndPass();
		$identity=$this->identity->getIdentity();
		$resource=$_array+["Identity"=>$identity];
	
		$response=$this->client->request('POST', 'GetAnalytics',['cert'=>[$this->certificate,$this->password],'json' => $resource]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
			return null;
		}

		return json_decode($response->getBody()->getContents(),true);

	}


	public function DeleteBusinesses($storeId){
		if(is_string($storeId)){
			$StoreIds[]=$storeId;
		} else {
			$StoreIds=$storeId;
		}

		$_array=[
				"StoreIds"=> $StoreIds,	
			 	"TrackingId"=> $this->trackingId];

		$this->checkCertAndPass();
		$identity=$this->identity->getIdentity();
		$resource=$_array+["Identity"=>$identity];
	
		$response=$this->client->request('POST', 'DeleteBusinesses',['cert'=>[$this->certificate,$this->password],'json' => $resource]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
			return null;
		}

		return json_decode($response->getBody()->getContents(),true);
	}


	public function CreateBulkChain(Chain $chain){
		$this->checkCertAndPass();
		$identity=$this->identity->getIdentity();
		
		$resource=[
			"ChainInfo"=>$chain->getChains(),
			"TrackingId"=>$this->trackingId,
			"Identity"=>$identity
		];
	
		$response=$this->client->request('POST', 'CreateBulkChain',['cert'=>[$this->certificate,$this->password],'json' => $resource]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
			return null;
		}

		return json_decode($response->getBody()->getContents(),true);

	}

	public function UpdateBulkChainInfo(Chain $chain){
		$this->checkCertAndPass();
		$identity=$this->identity->getIdentity();
		
		$resource=[
			"ChainInfo"=>$chain->getChains(),
			"TrackingId"=>$this->trackingId,
			"Identity"=>$identity
		];
	
		$response=$this->client->request('POST', 'UpdateBulkChainInfo',['cert'=>[$this->certificate,$this->password],'json' => $resource]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
			return null;
		}

		return json_decode($response->getBody()->getContents(),true);

	}

	
	*/
}

?>