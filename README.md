# Census Geocoder PHP Client Library (UNOFFICIAL)


## Installation

***NOTE:** Although this library may work with PHP 5.3^, it is only tested against PHP 7.2^ and we highly recommended to use PHP 7.2^. Additionally, 

### Install Client Library

**Install the Library via [Composer](http://getcomposer.org/):**

```shell
composer require cat_crash/census-php
```
or by adding composer package into your composer.json file
```shell
"cat_crash/census-php":"master@dev"
```

### Install Dependencies

Run the following from the root directory of the library:

```shell
composer install
```
if you going use logger for HTTP request - make sure that Monolog\Logger installed as well

## Example

```php
require('/path/to/your/vendor/autoload.php');
$census=new cat_crash\census_php\Census(['benchmark'=>'2020']);
$results=$census->search('100 mechanicsville rd, mechanicsville ');

/* or 
$results=$census->search([
	'street'=>'100 mechanicsville rd',
	'city'=>'mechanicsville',
	'state'=>'PA',
	'zip'=>18934
]);
*/

//get amount of returned matches:
//$results->getParsedAddresses()->count(); 

//get first match:
//$results->getParsedAddresses()->first(); 


$matches=$results->getParsedAddresses()->all();
foreach($matches as $value){
	echo $value->streetName; //will return MECHANICSVILLE
}

```

## Documentation
More info about Census Geocoder: https://geocoding.geo.census.gov/geocoder/Geocoding_Services_API.pdf


## Releasing
ToDo