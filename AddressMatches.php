<?php
namespace cat_crash\census_php;

Class AddressMatches {
	
	public $matchedAddress;
	public $coordinates;
	public $tigerLine;
	public $addressComponents;


	public function __construct(array $body){

		if(array_key_exists('matchedAddress', $body)){
			$this->matchedAddress=$body['matchedAddress'];
		} 

		if(array_key_exists('coordinates', $body)){
			$this->coordinates=new Coordinates($body['coordinates']);
		}

		if(array_key_exists('tigerLine', $body)){
			$this->tigerLine=new Tigerline($body['tigerLine']);
		}

		if(array_key_exists('addressComponents', $body)){
			$this->addressComponents=new AddressComponents($body['addressComponents']);
		}

	}


	public function __get($name)
    {
        if(property_exists($this->addressComponents, $name)){
        	return $this->addressComponents->{$name};
        } else {
        	throw new \Exception('Unknown property:'.$name);
        }
    }



}