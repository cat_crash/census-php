<?php
namespace cat_crash\census_php;

Class Tigerline {
		
	private $tigerLineId;
	private $side;


	public function __construct(array $body){
		
		foreach($body as $key=>$value){
			$this->setProperty($key,$value);
		} 
		return $this;
	}

	public function setProperty($name,$value){
		if(property_exists($this, $name)){
			$this->{$name}=$value;
		}
	}

}