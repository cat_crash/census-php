<?php
namespace cat_crash\census_php;

Class Input {
	
	public $benchmark;
	public $address;

	public function __construct(array $body){
		
		if(array_key_exists('benchmark', $body)){

			$this->benchmark=new Benchmark($body['benchmark']);
		
		} 

		if(array_key_exists('address', $body)){

			$this->address=new Address($body['address']);
		
		}

	}

}