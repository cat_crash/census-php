<?php
namespace cat_crash\census_php;

Class Result {
	
	public $input;
	public $addressMatches;
	public $count=0;

	public function __construct(array $body){
		
		if(array_key_exists('input', $body)){

			$this->input=new Input($body['input']);
		
		} 

		if(array_key_exists('addressMatches', $body)){
			
			foreach ($body['addressMatches'] as $value) {
				$this->count++;
				$this->addressMatches[]=new AddressMatches($value);
			}
		
		}

	}

	public function count(){
		return $this->count;
	}

	public function all(){
		return $this->addressMatches;
	}

	public function first(){
		if(!empty($this->addressMatches) && array_key_exists(0, $this->addressMatches)){
			return $this->addressMatches[0];
		} else {
			return null;
		}
	}

}